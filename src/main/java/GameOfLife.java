import inno.lemeshkov.homework7.game_of_life.logic.MainLoop;

import java.util.InvalidPropertiesFormatException;

public class GameOfLife {

    public static void main(String[] args) throws InvalidPropertiesFormatException {
        if (args.length != 5) {
            throw new InvalidPropertiesFormatException(
                    "Args must be 'input file name', 'output file name', 'step amount (int)', " +
                    "'type of run (mono/multi)', 'show (to show process/another to not)'"
            );
        }
        String inputFile = args[0];
        String outputFile = args[1];
        String typeOfRun = args[3];
        int maxStepAmount;
        try {
            maxStepAmount = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Third argument must be int" + e);
        }
        MainLoop mainLoop = new MainLoop(inputFile, outputFile, maxStepAmount);
        mainLoop.mainLoop(typeOfRun, args[4]);
    }
}
