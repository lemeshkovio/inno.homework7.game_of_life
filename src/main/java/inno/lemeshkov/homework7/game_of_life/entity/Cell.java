package inno.lemeshkov.homework7.game_of_life.entity;

import java.util.ArrayList;
import java.util.List;

public class Cell{
    private volatile CellStatus status = CellStatus.DEAD;
    private volatile CellStatus nextStatus = CellStatus.DEAD;
    private final int xCord;
    private final int yCord;
    private final List<Cell> neighbors = new ArrayList<>();

    /**
     * Create a simple dead Cell
     * @param xCord - Cell x cord
     * @param yCord - Cell y cord
     */
    public Cell(int xCord, int yCord) {
        this.xCord = xCord;
        this.yCord = yCord;
    }

    /**
     * calculate next cell status based on neighbors status
     */
    public void calculateNextStatus()  {
        int aliveNeighbors = aliveNeighborsAmount();
        if (aliveNeighbors == 2) {
            this.nextStatus = this.status;
        }
        if (aliveNeighbors == 3) {
            this.nextStatus = CellStatus.ALIVE;
        }
        if (aliveNeighbors < 2 || aliveNeighbors > 3) {
            this.nextStatus = CellStatus.DEAD;
        }
    }

    public int aliveNeighborsAmount(){
        return (int) this.neighbors.stream().filter(cell -> cell.getStatus().getDescription()
                .equals(CellStatus.ALIVE.getDescription())).count();
    }

    /**
     * update sell status
     */
    public void update() {
        this.status = this.nextStatus;
    }

    public int getX() {
        return xCord;
    }

    public int getY() {
        return yCord;
    }

    public CellStatus getStatus() {
        return this.status;
    }

    /**
     * set Cell status to alive
     */
    public void born() {
        this.status = CellStatus.ALIVE;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    public void addNeighbor(Cell neighbor) {
        this.neighbors.add(neighbor);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cell other = (Cell) obj;
        return status == other.status;
    }

    @Override
    public String toString() {
        return "Cell (status = " + status + ", x=" + xCord + ", y=" + yCord
                + ")";
    }
}
