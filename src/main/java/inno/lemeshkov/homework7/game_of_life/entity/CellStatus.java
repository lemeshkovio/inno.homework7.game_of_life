package inno.lemeshkov.homework7.game_of_life.entity;

public enum CellStatus {DEAD(" "), ALIVE("*");

    
    private final String description;

    CellStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
