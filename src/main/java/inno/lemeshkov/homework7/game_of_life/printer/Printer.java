package inno.lemeshkov.homework7.game_of_life.printer;

import inno.lemeshkov.homework7.game_of_life.entity.Cell;

import java.util.List;

public interface Printer {

    /**
     * print a desk
     * @param desk list of cells
     * @param x x size of desk
     * @param y y size of desk
     */
    void print(List<Cell> desk, int x, int y);
}
