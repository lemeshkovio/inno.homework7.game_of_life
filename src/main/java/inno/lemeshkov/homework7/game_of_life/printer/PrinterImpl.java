package inno.lemeshkov.homework7.game_of_life.printer;

import inno.lemeshkov.homework7.game_of_life.entity.Cell;

import java.util.List;

public class PrinterImpl implements Printer {


    @Override
    public void print(List<Cell> cells, int sizeX, int sizeY) {

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                if (x == sizeX - 1){
                    System.out.print(cells.get(y * sizeX + x).getStatus().getDescription());
                } else {
                    System.out.print(cells.get(y * sizeX + x).getStatus().getDescription() + " ");
                }

            }
            System.out.println();
        }
    }
}
