package inno.lemeshkov.homework7.game_of_life.writer;

import inno.lemeshkov.homework7.game_of_life.entity.Cell;

import java.io.*;
import java.util.List;

public class WriterImpl implements Writer {
    @Override
    public void write(String fileName, List<Cell> cells, int sizeX, int sizeY) {
        File file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (PrintWriter writer = new PrintWriter(file)) {
            for (int y = 0; y < sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    writer.write(cells.get(y * sizeX + x).getStatus().getDescription());
                }
                writer.write("\n");
            }
        } catch (IOException e) {
            System.err.println("Can't write in file\n" + e);
        }
    }
}
