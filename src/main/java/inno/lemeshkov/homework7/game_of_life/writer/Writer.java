package inno.lemeshkov.homework7.game_of_life.writer;

import inno.lemeshkov.homework7.game_of_life.entity.Cell;

import java.util.List;

public interface Writer {

    /**
     * Write a result of game in file
     * @param fileName name of target file
     * @param cells list of cells
     * @param sizeX x size of desk
     * @param sizeY y size of desk
     */
    void write(String fileName, List<Cell> cells, int sizeX, int sizeY);
}
