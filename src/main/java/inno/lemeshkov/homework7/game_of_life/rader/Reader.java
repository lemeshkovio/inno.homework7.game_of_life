package inno.lemeshkov.homework7.game_of_life.rader;

import java.util.List;

public interface Reader {

    /**
     * Read a file
     * @param filePath absolute file path
     * @return list of lines which formed from file content
     */
    List<String> read(String filePath);
}
