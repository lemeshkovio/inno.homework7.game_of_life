package inno.lemeshkov.homework7.game_of_life.rader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReaderImpl implements Reader{
    @Override
    public List<String> read(String filePath) {
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            return  lines.collect(Collectors.toList());
        } catch (IOException e) {
            System.err.println("Can't read file " + filePath + "\n" + e);
            return new ArrayList<>();
        }
    }
}
