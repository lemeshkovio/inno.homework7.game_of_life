package inno.lemeshkov.homework7.game_of_life.logic;

import inno.lemeshkov.homework7.game_of_life.entity.Cell;

import java.util.List;

public class CellThread extends Thread {
    private final List<Cell> cells;

    CellThread(List<Cell> cells) {
        this.cells = cells;
    }

    @Override
    public void run() {
        for (Cell cell : cells) {
            cell.calculateNextStatus();
        }
        for (Cell cell : cells) {
            cell.update();
        }
    }
}
