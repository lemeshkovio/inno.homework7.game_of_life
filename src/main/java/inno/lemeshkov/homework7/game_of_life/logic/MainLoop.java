package inno.lemeshkov.homework7.game_of_life.logic;

import inno.lemeshkov.homework7.game_of_life.entity.Cell;
import inno.lemeshkov.homework7.game_of_life.entity.Desk;
import inno.lemeshkov.homework7.game_of_life.printer.Printer;
import inno.lemeshkov.homework7.game_of_life.printer.PrinterImpl;
import inno.lemeshkov.homework7.game_of_life.writer.Writer;
import inno.lemeshkov.homework7.game_of_life.writer.WriterImpl;

import java.util.List;
import java.util.stream.Collectors;

public class MainLoop {
    private static final String ALIVE = "*";
    private static final String PATH_INPUT = "/Users/ivanlemeskov/IdeaProjects/homework7/src/main/resources/input/";
    private static final String PATH_OUTPUT = "/Users/ivanlemeskov/IdeaProjects/homework7/src/main/resources/output/";
    private static final String RANDOM_START = "random";
    private static final String MONO_THREAD = "mono";
    public static final String MAGIC_WORD_TO_CLEAR_CONSOLE = "\033[H\033[2J";

    private final int stepAmount;
    private final String inputFile;
    private final String outputFile;

    /**
     * Create mainLoop with 3 parameters
     *
     * @param inputFile  name of input file
     * @param outputFile name of output file
     * @param stepAmount amount of steps
     */
    public MainLoop(String inputFile, String outputFile, int stepAmount) {
        this.stepAmount = stepAmount;
        this.inputFile = PATH_INPUT + inputFile;
        this.outputFile = PATH_OUTPUT + outputFile;
    }

    /**
     * Main loop that does all the fun
     *
     * @param typeOfStart mono/multi choose type of app run
     */
    public synchronized void mainLoop(String typeOfStart, String show) {
        Printer printer = new PrinterImpl();
        Desk desk;
        if (inputFile.contains(RANDOM_START)) {
            desk = new Desk();
        } else {
            desk = fileStart(this.inputFile);
        }
        List<Cell> cells = desk.getCells();
        int aliveLast = 0;
        for (int step = 0; step < stepAmount; step++) {

            int aliveCurrent = (int) cells.stream().filter(cell -> cell.getStatus().
                    getDescription().equals(ALIVE)).count();
            if (show.equals("show")) {
                System.out.println("step " + step + "|Got alive cells:" + aliveCurrent);
                printer.print(cells, desk.getX(), desk.getY());
            }
            if (typeOfStart.contains(MONO_THREAD)) {
                cells.forEach(Cell::calculateNextStatus);
                aliveLast = aliveCurrent;
                cells.forEach(Cell::update);
            } else {
                CellThread thread1 = new CellThread(cells);
                thread1.start();
            }
            if (show.equals("show")) {
                clearScreen();
            }
        }

        System.out.println("Last Step: " + stepAmount + "| Last alive cell count: "
                + aliveLast + "|\n");
        Writer writer = new WriterImpl();
        writer.write(outputFile, cells, desk.getX(), desk.getY());
    }

    private static Desk fileStart(String fileName) {
        return new Desk(fileName);
    }

    private static void clearScreen() {
        try {
            Thread.sleep(120);
        } catch (InterruptedException e) {
            System.err.println("Can't clear screen\n" + e);
            return;
        }
        System.out.print(MAGIC_WORD_TO_CLEAR_CONSOLE);
        System.out.flush();
    }
}
