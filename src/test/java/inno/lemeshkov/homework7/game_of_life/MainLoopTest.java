package inno.lemeshkov.homework7.game_of_life;

import inno.lemeshkov.homework7.game_of_life.logic.MainLoop;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MainLoopTest {
    public static final String INPUT = "test.txt";
    public static final String OUTPUT = "test.txt";
    public static final String MONO = "monoStart";
    public static final String MULTI = "multiStart";
    public static final String UNSHOW = "unShow";
    public static final int STEP_AMOUNT = 10000;

    private MainLoop mainLoop = new MainLoop(INPUT, OUTPUT, STEP_AMOUNT);
    @Test
    void test_mainLoop_MultiThreadShouldWorkFasterThenMonoThread() {

        long startTime = System.nanoTime();
        mainLoop.mainLoop(MONO,UNSHOW);
        long monoTime = System.nanoTime() - startTime;
        System.out.printf("monoThread takes %.2f sec.\n",monoTime/1000000000.0);

        startTime = System.nanoTime();
        mainLoop.mainLoop(MULTI,UNSHOW);
        long multiTime = System.nanoTime() - startTime;
        System.out.printf("multiThread takes %.2f sec.\n",multiTime/1000000000.0);
        assertTrue(multiTime < monoTime);
    }
}


